package BenutzerverwaltungV10;

import java.io.InputStream;
import java.util.Scanner;
import java.io.Console;

public class BenutzerverwaltungV10 {

    public static void main(String[] args) {

        Benutzerverwaltung.start();

    }
}

class Benutzerverwaltung{
    public static void start(){
        BenutzerListe benutzerListe = new BenutzerListe();

        benutzerListe.insert(new Benutzer("Paula", "paula"));
        benutzerListe.insert(new Benutzer("Adam37", "adam37"));
        benutzerListe.insert(new Benutzer("Darko", "darko"));


        // Hier bitte das Menü mit der Auswahl
        //  - Anmelden
        //  - Registrieren
        // einfügen, sowie die entsprechenden Abläufe:
        // Beim Registrieren 2x das Passwort einlesen und vergleichen,
        // das neue Benutzerobjekt erzeugen und in die Liste einfügen.
        // Beim Anmelden (max. 3 Versuche) name und passwort einlesen,
        // in der Liste nach dem Namen suchen und das eingegebene Passwort
        // mit dem gespeicherten vergleichen.

        boolean systemLäuft =true;
        while(systemLäuft){
            System.out.println("Bitte wahlen Sie: \n(1) - Anmelden \n(2) - Registrieren");
            Scanner sc = new Scanner(System.in);
            System.out.println("Ihre Auswahl:");
            String auswahl = sc.nextLine();
            switch (auswahl){
                case "1":
                   int  i=0;
                   while(i<3) { System.out.println("Name:");
                    String benutzer = sc.nextLine();
                    System.out.println("Passwort:");
                    String passwort = sc.nextLine();
                    i++;
                    if (benutzerListe.select(benutzer).equals(new Benutzer(benutzer, passwort).toString())) {
                        System.out.println("Hallo " + benutzer+ "! Sie sind erfolig angemeldet\n");
                        System.out.print("System herunterfahren? [j/n] ");
                        systemLäuft = !sc.next().equals("j");
                        break;
                    }else if (i==3){
                        System.out.println("Oops! Sie haben die maximalle Versuche erreicht. \n");
                    }else {
                        System.out.println("Falsche Name oder Passwort, Sie haben max 3 Versuche.");
                    }
                   }
                    break;
                case "2":
                    System.out.println("Name:");
                    String neueBenutzer = sc.nextLine();
                    System.out.println("Passwort:");
                    String neuePasswort = sc.nextLine();
                    System.out.println("Wiederholen Sie Ihre Passwort:");
                    String passwortWiederholung = sc.nextLine();
                    if (neuePasswort.equals(passwortWiederholung)){
                        benutzerListe.insert(new Benutzer(neueBenutzer,neuePasswort));
                        System.out.println("Hallo " + neueBenutzer+ "! Sie wurden erfolig registriert\n");
                        System.out.print("System herunterfahren? [j/n] ");
                        systemLäuft = !sc.next().equals("j");
                    } else
                        System.out.println("Oop! Passworten nicht gleich, versuchen Sie es nochmal!\n");
                    break;
                default:
                    System.out.println("Oops! Sie mussen 1 oder 2 wahlen, versuchen Sie es nochmal!\n");

            }
        }

    }

}

class BenutzerListe{
    private Benutzer first;
    private Benutzer last;
    public BenutzerListe(){
        first = last = null;
    }
    public void insert(Benutzer b){
        // Sicherheitshalber setzen wir
        // den Nachfolger auf null:
        b.setNext(null);
        if(first == null){
            first = last = b;
        }
        else{
            last.setNext(b);
            last = b;
        }
    }
    public String select(){
        String s = "";
        Benutzer b = first;
        while(b != null){
            s += b.toString() + '\n';
            b = b.getNext();
        }
        return s;
    }
    public String select(String name){
        Benutzer b = first;
        while(b != null){
            if(b.hasName(name)){
                return b.toString();
            }
            b = b.getNext();
        }
        return "";
    }

    public boolean delete(String name){
        // ...
        return true;
    }
}

class Benutzer{
    private String name;
    private String passwort;

    private Benutzer next;

    public Benutzer(String name, String pw){
        this.name = name;
        this.passwort = pw;

        this.next = null;
    }

    public boolean hasName(String name){
        return name.equals(this.name);
    }

    public String toString(){
        String s = "";
        s += name + " ";
        s += passwort;
        return s;
    }

    public Benutzer getNext(){
        return next;
    }

    public void setNext(Benutzer b){
        next = b;
    }
}