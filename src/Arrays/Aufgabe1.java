package Arrays;

public class Aufgabe1 {

    public static void main(String[] arg) {

        int[] list;
        list = new int[10];

        System.out.println("Hier ist deine Liste: ");
        for (int i = 0; i < list.length; i++) {
            list[i] = i;
            System.out.print(list[i]);
            if ( i != list.length -1)
                System.out.print(",");
        }

    }
}
