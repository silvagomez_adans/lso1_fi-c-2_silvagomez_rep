package Arrays;
import java.util.Scanner;

import java.util.Arrays;

import static java.lang.System.out;

public class Aufgabe4_Lotto {
    public static void main (String[] arg){
        int [] lotto;
        int zahl;
        Scanner sc= new Scanner(System.in);
        lotto = new int [] {3,7,12,18,37,42};

        //Richtige zahlen 3, 7, 12, 18, 37 und 42

        System.out.println( Arrays.toString(lotto));
        out.print("Geben Sie eine Zahl (12) oder (13) ein: ");
        zahl= sc.nextInt();
        if (zahl==lotto[2]){
            out.println("Die Zahl 12 ist in der Ziehung enthalten");
        }else if (zahl==13){
            out.println("Die Zahl 13 ist nicht in der Ziehung enthalten.");
        }else out.println("Ungültige zahl");

    }
}
