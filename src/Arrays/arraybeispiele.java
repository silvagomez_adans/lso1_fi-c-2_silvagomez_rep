package Arrays;

public class arraybeispiele {
    public static void main (String [] args ){

        int [] zlist;
        zlist= new int [100];

        //oder int [] zlist = new int [5];

        zlist [0]= 10;
        zlist [1]= 11;
        zlist [2]= 12;
        zlist [3]= 13;
        zlist [4]= 14;

        // para trabajar el array ustilizamos un
        // for (int i=0 ; i < 5 ; i++){
        // a1;
        // a2
        // a...
        // an;
        // }
        // oder
        //int i= 0;
        // while ( i < 5){
        //a1;
        //a2;
        //.. an;
        // i++   }

        int inialValue= 0;
        for (int i=0 ; i<100 ; i++) {
            zlist[i] = inialValue;
            inialValue++;
        }
        // oder   for (int i=0 ; i<100 ; i++) {
        //           zlist[i] = i;
        //           }

        for (int i=0; i< 100; i++ ){
            System.out.print(zlist[i]);
            //for (int i=0; i< zlist.lengt; i++ ){  ------ NOTA : en este caso no hay limite de elementos dentro del array
            //            System.out.print(zlist[i] + ",");
            // ::::Y ESTA ES LA FORMA QUE EL PROFESOR QUIERE:::::::
            if ( i != zlist.length -1)
                System.out.print(",");
            // esto es para agregar una coma y solo el penultimo valor debe tener
        }

    }
}
