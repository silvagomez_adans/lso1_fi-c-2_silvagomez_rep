package Fahrkartenautomat;

import java.text.DecimalFormat;
import java.util.Scanner;

public class Fahrkartenautomat07 {

    public static void main(String[] args) {

        double amountToBePaid;
        double amountPaid;

        amountToBePaid = ticketchoice() * amountOfTickets();
        amountPaid = paying(amountToBePaid);

        // Printing the tickets
        // -----------------
        System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 7; i++) {
            System.out.print("=");
            try {
                Thread.sleep(150);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        System.out.println("\n\n");

        // Calculation of the bill
        // -------------------------------
        printChange(amountPaid, amountToBePaid);

        System.out.println("\nVergessen Sie nicht, den Fahrschein\n" +
                "vor Fahrtantritt entwerten zu lassen!\n" +
                "Wir wünschen Ihnen eine gute Fahrt.");
        main(args); //main wird wieder angeruft

    }

    public static double ticketchoice() {
        double[] prices = new double[]{
                2.90, 3.30, 3.60, 1.90, 8.60, 9.00, 9.60, 23.50, 24.30, 24.90
        };
        String[] designations = new String[]{
                "Einzelfahrschein Berlin AB",
                "Einzelfahrschein Berlin BC",
                "Einzelfahrschein Berlin ABC",
                "Kurzstrecke",
                "Tageskarte Berlin AB",
                "Tageskarte Berlin BC",
                "Tageskarte Berlin ABC",
                "Kleingruppen-Tageskarte Berlin AB",
                "Kleingruppen-Tageskarte Berlin BC",
                "Kleingruppen-Tageskarte Berlin ABC",
        };
        System.out.println("Whählen Sie ihre Wunschfarkarte für Berlin ABC aus: \n");

        for (int i = 0; i < designations.length; i++) {
            System.out.println("(" + (i + 1) + ") für " + designations[i] + " [" + prices[i] + "]");
        }

        System.out.println("\nGewünschte Karte: ");

        Scanner sc = new Scanner(System.in);
        int choice = sc.nextInt();
        if (choice == 123) {
            System.exit(0);
        } else if (choice <= prices.length) {
            return prices[choice - 1];
        }
        System.out.println("Ungültiges Wahl bitte versuchen Sie noch mal");
        return ticketchoice();
    }


    public static double paying(double amountToBePaid) {
        Scanner sc = new Scanner(System.in);
        DecimalFormat f = new DecimalFormat("#0.00");
        double totalAmountPaid = 0.0;
        while (totalAmountPaid < amountToBePaid) {
            double bill = amountToBePaid - totalAmountPaid;
            System.out.println("Noch zu zahlen: " + (f.format(bill)) + " EURO ");
            System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
            double amountPaidInCoins = sc.nextDouble();
            totalAmountPaid = totalAmountPaid + amountPaidInCoins;

            // (eingezahlterGesamtbetrag = eingezahlterGesamtbetrag + eingeworfeneMünze) = (eingezahlterGesamtbetrag += eingeworfeneMünze)
            // esto significa lo mismo (a+=b)=(a=a+b)
        }
        return totalAmountPaid;

    }

    public static double amountOfTickets() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Wie viele möchten Sie kaufen : ");
        return sc.nextInt();
    }


    public static void printChange(double totalAmountPaid, double amountToBePaid) {
        DecimalFormat f = new DecimalFormat("#0.00");
        double change = totalAmountPaid - amountToBePaid;
        if (change > 0.0) {
            System.out.println("Der Rückgabebetrag in Höhe von " + f.format(change) + " EURO");
            System.out.println("wird in folgenden Münzen ausgezahlt:");

            while (change >= 2.0) // 2 EURO-Münzen
            {
                System.out.println("2 EURO");
                change -= 2.0;
            }
            while (change >= 1.0) // 1 EURO-Münzen
            {
                System.out.println("1 EURO");
                change -= 1.0;
            }
            while (change >= 0.5) // 50 CENT-Münzen
            {
                System.out.println("50 CENT");
                change -= 0.5;
            }
            while (change >= 0.2) // 20 CENT-Münzen
            {
                System.out.println("20 CENT");
                change -= 0.2;
            }
            while (change >= 0.10) // 10 CENT-Münzen
            {
                System.out.println("10 CENT");
                change -= 0.10;
            }
            while (change >= 0.05)// 5 CENT-Münzen
            {
                System.out.println("5 CENT");
                change -= 0.05;
            }
        }
    }


}