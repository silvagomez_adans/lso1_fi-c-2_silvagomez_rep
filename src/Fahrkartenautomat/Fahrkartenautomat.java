package Fahrkartenautomat;

import java.text.DecimalFormat;
import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {

        DecimalFormat f = new DecimalFormat("#0.00");
        Scanner tastatur = new Scanner(System.in);
        // Achtung!!!!
        // Hier wird deutlich, warum double nicht für Geldbeträge geeignet ist.
        // =======================================
        // aca se definen los significados de mis individuos a usar en los procesos
        double zuZahlenderBetrag;
        double eingezahlterGesamtbetrag;
        double eingeworfeneMünze;
        double rückgabebetrag;
        // Die Eingabe erfolgt anwenderfreudlich mit Dezimalpunkt: just testing
        double eingegebenerBetrag;
        double ticketmenge;
        double rechnung;

        // Den zu zahlenden Betrag ermittelt normalerweise der Automat
        // aufgrund der gewählten Fahrkarte(n).
        // -----------------------------------
        // transformed all the processes into funtions to make then work separatly
        zuZahlenderBetrag = naming();
        ticketmenge = cuantas();
        zuZahlenderBetrag = ticketmenge * zuZahlenderBetrag;
        // Geldeinwurf
        // -----------
        eingezahlterGesamtbetrag = paying(zuZahlenderBetrag);

        // Fahrscheinausgabe
        // -----------------
        System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++)
        {
            System.out.print("=");
            try {
                Thread.sleep(250);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        System.out.println("\n\n");

        // Rückgeldberechnung und -Ausgabe
        // -------------------------------
        vuelto(eingezahlterGesamtbetrag, zuZahlenderBetrag);

        System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                "vor Fahrtantritt entwerten zu lassen!\n"+
                "Wir wünschen Ihnen eine gute Fahrt.");
    }


    public static double paying(double zuZahlenderBetrag) {
        Scanner tastatur = new Scanner(System.in);
        DecimalFormat f = new DecimalFormat("#0.00");
        double eingezahlterGesamtbetrag = 0.0;
        while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
        {
            double rechnung = zuZahlenderBetrag - eingezahlterGesamtbetrag;
            System.out.println("Noch zu zahlen: " + (f.format(rechnung)) + " EURO ");
            System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
            double eingeworfeneMünze = tastatur.nextDouble();
            eingezahlterGesamtbetrag = eingezahlterGesamtbetrag + eingeworfeneMünze;

            // (eingezahlterGesamtbetrag = eingezahlterGesamtbetrag + eingeworfeneMünze) = (eingezahlterGesamtbetrag += eingeworfeneMünze)
            // esto significa lo mismo (a+=b)=(a=a+b)
        }
        return eingezahlterGesamtbetrag;

    }

    public static double naming() {
        Scanner tastatur = new Scanner(System.in);
        System.out.print("Zu zahlender Betrag (EURO): ");
        double zuZahlenderBetrag = tastatur.nextDouble();
        return zuZahlenderBetrag;
    }


    public static double cuantas(){

        Scanner tastatur = new Scanner(System.in);
        System.out.print("Wie viele möchten Sie kaufen : ");
        double ticketmenge = tastatur.nextInt();
        return ticketmenge;
    }


    public static void vuelto(double eingezahlterGesamtbetrag, double zuZahlenderBetrag) {
        double rückgabebetrag;
        DecimalFormat f = new DecimalFormat("#0.00");

        rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
        if (rückgabebetrag > 0.0) {
            System.out.println("Der Rückgabebetrag in Höhe von " + f.format(rückgabebetrag) + " EURO");
            System.out.println("wird in folgenden Münzen ausgezahlt:");

            while (rückgabebetrag >= 2.0) // 2 EURO-Münzen
            {
                System.out.println("2 EURO");
                rückgabebetrag -= 2.0;
            }
            while (rückgabebetrag >= 1.0) // 1 EURO-Münzen
            {
                System.out.println("1 EURO");
                rückgabebetrag -= 1.0;
            }
            while (rückgabebetrag >= 0.5) // 50 CENT-Münzen
            {
                System.out.println("50 CENT");
                rückgabebetrag -= 0.5;
            }
            while (rückgabebetrag >= 0.2) // 20 CENT-Münzen
            {
                System.out.println("20 CENT");
                rückgabebetrag -= 0.2;
            }
            while (rückgabebetrag >= 0.10) // 10 CENT-Münzen
            {
                System.out.println("10 CENT");
                rückgabebetrag -= 0.10;
            }
            while (rückgabebetrag >= 0.05)// 5 CENT-Münzen
            {
                System.out.println("5 CENT");
                rückgabebetrag -= 0.05;
            }
        }
    }
}
