import java.util.Scanner;

public class BMI {

    public static void main( String []args){
        //variables
        Scanner sc= new Scanner(System.in);
        String geschlecht;
        double körpergewicht, körpergröße, körpergröße_m2, bmi;
        //Eingabe
        System.out.println("Geben Sie Ihre Geschlecht an: ");
        geschlecht= sc.next();
        System.out.print("Bitte geben Sie Ihre Körpergrößer in [cm]: ");
        körpergröße= sc.nextInt();
        System.out.println("Bitte geben Sie Ihre Körpergewicht in [kg]: ");
        körpergewicht= sc.nextInt();
        körpergröße_m2= (körpergröße * körpergröße) /10000;
        //Rechnung
        bmi= Math.round(körpergewicht / körpergröße_m2);
        //Result
        System.out.println( "Ihre BMI beträgt: " + bmi);

        //Kategorisierung
        if (bmi<20 && geschlecht.equalsIgnoreCase("m") ){
            System.out.println("Sie haben ein Untergewicht");
        }else if (bmi<19 && geschlecht.equalsIgnoreCase("w")){
            System.out.println("Sie haben ein Untergewicht");
        }else if (bmi>=20 && bmi<=25 && geschlecht.equalsIgnoreCase("m")){
            System.out.println("Sie haben ein normales Gewicht");
        }else if (bmi>=20 && bmi<=24 && geschlecht.equalsIgnoreCase("w")){
            System.out.println("Sie haben ein normales Gewicht");
        }else if (bmi>25 && geschlecht.equalsIgnoreCase("m")){
            System.out.println("Sie haben ein Übergewicht");
        }else if (bmi>24 && geschlecht.equalsIgnoreCase("w")){
            System.out.println("Sie haben ein Übergewicht");
        } else System.out.println("Ein Wert ist nicht gültig");
    }
}
