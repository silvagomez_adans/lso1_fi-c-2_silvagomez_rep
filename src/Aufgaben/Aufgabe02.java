package Aufgaben;

public class Aufgabe02 {

    public static void main(String[] args) {
        System.out.printf("\n%s", "0!   =                        = 1");
        System.out.printf("\n%s", "1!   = 1                      = 1");
        System.out.printf("\n%s", "2!   = 1 * 2                  = 2");
        System.out.printf("\n%s", "3!   = 1 * 2 * 3              = 6");
        System.out.printf("\n%s", "4!   = 1 * 2 * 3 * 4          = 24");
        System.out.printf("\n%s\n", "4!   = 1 * 2 * 3 * 4 * 5      = 120");
    }

}
