package Aufgaben;

public class Aufgabe03 {
    public static void main(String[] args){
        System.out.printf( "\n%s|", "Fahrenheit  ");
        System.out.printf( "%s\n", "   Celsius"  );
        System.out.printf( "%s\n", "-----------------------"  );
        System.out.printf( "%-12s", "-20"  );
        System.out.printf( "|%s", "    -28.89" );
        System.out.printf( "\n%-12s|", "-10");
        System.out.printf( "%s", "    -23.33" );
        System.out.printf( "\n%-12s|", "+0");
        System.out.printf( "%s", "    -17.78" );
        System.out.printf( "\n%-12s|", "+20");
        System.out.printf( "%s", "     -6.67" );
        System.out.printf( "\n%-12s|", "+30");
        System.out.printf( "%s", "     -1.11" );
    }
}
