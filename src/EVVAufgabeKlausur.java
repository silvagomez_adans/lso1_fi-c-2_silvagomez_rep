import java.util.Scanner;

public class EVVAufgabeKlausur {

    public static void main (String[]args){

        Scanner sc= new Scanner(System.in);

        //Datos de usuario
        System.out.println("Distancia recorrida [km]: ");
        double distanciaenKm= sc.nextDouble();
        System.out.println("Tiempo que necesito [hh:mm:ss]: ");
        String distanciaTiempo= sc.next();

        //Calculo
        String[] distanciaHrMinSec = distanciaTiempo.split("\\:");
        int h= Integer.valueOf(distanciaHrMinSec[0]);
        int min= Integer.valueOf(distanciaHrMinSec[1]);
        int sec= Integer.valueOf(distanciaHrMinSec[2]);

        double segundos = 60.0 * 60.0 * h + 60.0 * min + sec;

        //Resultado
        System.out.println("En medida usted recorrio:  " + velocidadmedidaKmh(segundos, distanciaenKm) + "km/h");
    }
    static double velocidadmedidaKmh( double segundos, double distanciaenKm){
        double velocidadmediaKmh= distanciaenKm / segundos *60.0 * 60.0;
        return velocidadmediaKmh;

    }

}