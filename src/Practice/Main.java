package Practice;

public class Main {
    public static void main(String[]args){

        Persona objeto = new Persona();
        objeto.setId(10);
        objeto.setNombre("Juan");
        objeto.setApellido("Perez");
        System.out.println("Sus Datos son los siguientes");
        System.out.println("El ID es: " + objeto.getId());
        System.out.println("El Nombre es:" + objeto.getNombre());
        System.out.println("El Apellido es:" + objeto.getApellido());

    }

}
