package Practice;
import java.util.Scanner;

public class Persona {
    Scanner sc= new Scanner(System.in);
     private int id;
     private String nombre;
     private String apellido;

   public Persona() {

   }
   public Persona(int id, String nombre, String apellido){

       this.id = id;
       this.nombre = nombre;
       this.apellido = apellido;
   }
   public void setId(int id){
       System.out.println("ID:");
       this.id =sc.nextInt();
   }

    public int getId() {
        return id;
    }

    public void setApellido(String apellido) {
        System.out.println("Apellido:");
        this.apellido = sc.next();
    }

    public String getApellido() {
        return apellido;
    }

    public void setNombre(String nombre) {
        System.out.println("Nombre:");
        this.nombre = sc.next();
    }

    public String getNombre() {
        return nombre;
    }
}
