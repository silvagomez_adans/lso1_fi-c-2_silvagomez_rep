package Aufgabe_User;

public class User {
    private String name;
    private String email;
    private String password;
    private String birthday;
    private int loginnumbers;
    private double high;
    private String loginStatus;
    private int age;

    public User(String name, String email, String password, String birthday, double high, String loginStatus, int age){
        this.name = name;
        this.email = email;
        this.password = password;
        this.birthday= birthday;
        this.loginnumbers = 2;
        this.high = high;
        this.loginStatus = loginStatus;
        this.age = age;


    }
    public void setName (String name) {
        this.name = name;
    }
    public String getName() {
        return this.name;
    }
    public void setEmail(String email){
        this.email = email;
    }
    public String getEmail(){
        return this.email;
    }
    public void setPassword(String password){
        this.password = password;
    }
    public String getPassword(){
        return this.password;
    }
    public void setBirthday(String birthday){
        this.birthday= birthday;
    }
    public String getBirthday(){
        return this.birthday;
    }

    public void setHigh(double high) {
        this.high = high;
    }

    public double getHigh() {
        return high;
    }

    public void setLoginStatus(String loginStatus) {
        this.loginStatus = loginStatus;
    }

    public String getLoginStatus() {
        return loginStatus;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getAge() {
        return age;
    }

    public void loginNumbers() {
        System.out.println("User logged in "+ loginnumbers + " times today.");
    }
}

