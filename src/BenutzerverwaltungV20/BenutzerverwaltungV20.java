package BenutzerverwaltungV20;

import java.util.Arrays;
import java.util.Scanner;

public class BenutzerverwaltungV20 {

    public static void main(String[] args) {
            Benutzerverwaltung.start();
    }
}

class Benutzerverwaltung{
    private static BenutzerListe benutzerListe;

    public static void start() {
        benutzerListe = new BenutzerListe();

        benutzerListe.insert(new Benutzer("Paula", Crypto.encrypt("paula".toCharArray())));
        benutzerListe.insert(new Benutzer("Adam37", Crypto.encrypt("adam37".toCharArray())));
        benutzerListe.insert(new Benutzer("Darko", Crypto.encrypt("darko".toCharArray())));
        benutzerListe.insert(new Benutzer("Admin", new char[]{36, 61, 72, 72, 75, 13, 14, 15}));

        // Hier bitte das Menü mit der Auswahl
        //  - Anmelden
        //  - Registrieren
        // einfügen, sowie die entsprechenden Abläufe:
        // Beim Registrieren 2x das Passwort einlesen und vergleichen,
        // das neue Benutzerobjekt erzeugen und in die Liste einfügen.
        // Beim Anmelden (max. 3 Versuche) name und passwort einlesen,
        // in der Liste nach dem Namen suchen und das eingegebene Passwort
        // mit dem gespeicherten vergleichen.

        boolean systemLäuft =true;
        while(systemLäuft){
            System.out.println("Bitte wahlen Sie: \n(1) - Anmelden \n(2) - Registrieren\n(3) - Benutzer Löschen");
            Scanner sc = new Scanner(System.in);
            System.out.println("Ihre Auswahl:");
            String auswahl = sc.nextLine();
            switch (auswahl){
                case "1":
                    int  i=0;
                    while(i<3) { System.out.println("Name:");
                        String benutzer = sc.nextLine();
                        System.out.println("Passwort:");
                        String passwort = sc.nextLine();
                        i++;
                        if (authenticate(benutzer, Crypto.encrypt(passwort.toCharArray()))) {
                            System.out.println("Hallo " + benutzer+ "! Sie sind erfolig angemeldet\n");
                            System.out.print("System herunterfahren? [j/n] ");
                            systemLäuft = !sc.next().equals("j");
                            break;
                        }else if (i==3){
                            System.out.println("Oops! Sie haben die maximalle Versuche erreicht. \n");
                        }else {
                            System.out.println("Falsche Name oder Passwort, Sie haben max 3 Versuche.");
                        }
                    }
                    break;
                case "2":
                    System.out.println("Name:");
                    String neueBenutzer = sc.nextLine();
                    System.out.println("Passwort:");
                    String neuePasswort = sc.nextLine();
                    System.out.println("Wiederholen Sie Ihre Passwort:");
                    String passwortWiederholung = sc.nextLine();
                    if (neuePasswort.equals(passwortWiederholung)){
                        benutzerListe.insert(new Benutzer(neueBenutzer,Crypto.encrypt(neuePasswort.toCharArray())));
                        System.out.println("Hallo " + neueBenutzer+ "! Sie wurden erfolig registriert\n");
                        System.out.print("System herunterfahren? [j/n] ");
                        systemLäuft = !sc.next().equals("j");
                    } else
                        System.out.println("Oop! Passworten nicht gleich, versuchen Sie es nochmal!\n");
                    break;
                case "3":
                    System.out.println("Bitte geben sie die Name von Benutzer an:");
                    String name = sc.nextLine();
                    if (benutzerListe.delete(name)) {
                        System.out.println("Sie wurden erfolig gelöscht ");
                    } else {
                        System.out.println("Nicht gelöscht, versuchen Sie nochmal");
                    }
                break;


                default:
                    System.out.println("Oops! Sie mussen 1, 2 oder 3 wahlen, versuchen Sie es nochmal!\n");

            }
        }
    }

    public static boolean authenticate(String name, char[] cryptoPw) {
        Benutzer b = benutzerListe.getBenutzer(name);
        if(b != null) {
            if(b.hasPasswort(cryptoPw)){
                return true;
            }
        }
        return false;
    }
}

class BenutzerListe {
    private Benutzer first;
    private Benutzer last;
    public BenutzerListe(){
        first = last = null;
    }

    public Benutzer getBenutzer(String name) {
        Benutzer b = first;
        while(b != null){
            if(b.hasName(name)) {
                return b;
            }
            b = b.getNext();
        }
        return null;
    }

    public void insert(Benutzer b) {
        // Sicherheitshalber setzen wir
        // den Nachfolger auf null:
        b.setNext(null);
        if(first == null) {
            first = last = b;
        }
        else {
            last.setNext(b);
            last = b;
        }
    }

    public String select() {
        String s = "";
        Benutzer b = first;
        while(b != null){
            s += b.toString() + '\n';
            b = b.getNext();
        }
        return s;
    }

    public String select(String name) {
        Benutzer b = first;
        while(b != null){
            if(b.hasName(name)) {
                return b.toString();
            }
            b = b.getNext();
        }
        return "";
    }

    public boolean delete(String name) {
        Benutzer b = first;
        if (b.hasName(name)) {
            first = first.getNext();
            return true;
        }
        while(b != null && b.getNext() != null){
            if(b.getNext().hasName(name)){
                if (b.getNext() == last) {
                    last = b;
                }
                b.setNext(b.getNext().getNext());
                return true;
            }
            b = b.getNext();
        }
        return false;
    }
}

class Benutzer {
    private String name;
    private char[] passwort;  // Verschlüsselt!

    private Benutzer next;

    public Benutzer(String name, char[] pw){
        this.name = name;
        this.passwort = pw;

        this.next = null;
    }

    public boolean hasName(String name){
        return name.equals(this.name);
    }

    public boolean hasPasswort(char[] cryptoPw){
        return Arrays.equals(this.passwort, cryptoPw);
    }

    public String toString(){
        String s = "";
        s += name + " ";
        s += passwort;
        return s;
    }

    public Benutzer getNext(){
        return next;
    }

    public void setNext(Benutzer b){
        next = b;
    }
}

class Crypto {
    private static int cryptoKey = 65500;

    public static char[] encrypt(char[] s) {
        char[] encrypted = new char[s.length];
        for(int i = 0; i < s.length; i++) {
            encrypted[i] = (char)((s[i] + cryptoKey) % 128);
        }
        return encrypted;
    }
}

