package Strukturiertes_Programmieren;

import java.util.Scanner;
 public class Rabattsystem {
     //Der Hardware-Großhändler führt ein Rabattsystem ein: Liegt der Bestellwert zwischen 0 und
     //100 €, erhält der Kunde einen Rabatt von 10 %. Liegt der Bestellwert höher, aber insgesamt
     //nicht über 500 €, beträgt der Rabatt 15 %, in allen anderen Fällen beträgt der Rabatt 20 %.
     //Nach Eingabe des Bestellwertes soll der ermäßigte Bestellwert (incl. MwSt.) berechnet und
     //ausgegeben werden.

        public static void main( String [] args){

            Scanner sc= new Scanner(System.in);
            double preis;

            System.out.print("Wie viel möchten sie bezahle? :" );
            preis= sc.nextInt();
            double total= Math.round(preis - rabatt(preis));
            double steuer= (total*0.19);
            double totalohne= (total - steuer);
            System.out.println("Total ohne steuer= " + totalohne );
            System.out.println("Total= " + total +" (incl. MwSt.)");
        }
        public static double rabatt (double preis  ){

            if (preis>0 && preis <100){
                return preis * 0.10;
            }else if (preis>=100 && preis<500) {
                return preis * 0.15;
            }else return preis* 0.20;
        }

    }

